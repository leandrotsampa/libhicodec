#===============================================================================
# export variable
#===============================================================================
SRC_DIR := $(CURDIR)

CFLAGS := -Wl,--unresolved-symbols=ignore-in-shared-libs -Wall -D_GNU_SOURCE
SYS_LIBS := -lpthread -lm -ldl
HI_LIBS := -lhi_common -lhi_msp

HI_DEPEND_LIBS := $(SYS_LIBS) $(HI_LIBS)

#===============================================================================
# local variable
#===============================================================================
OUT     := libhicodec.so
VERSION := \"$(shell date "+%Y.%m-%d (Build: %H%M%S)")\"

LOCAL_SRCS := hicodec.c

#===============================================================================
# rules
#===============================================================================
default: $(OUT)

strip: $(OUT)
	@strip --strip-all $(OUT)

$(OUT): clean version
	@gcc -shared -fPIC -DPIC $(CFLAGS) $(HI_DEPEND_LIBS) -o $(OUT) $(LOCAL_SRCS)

clean:
	@rm -f $(OUT)

version:
	@echo "#define HICODEC_VERSION $(VERSION)">version.h