#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>
#include <errno.h>
#include <netinet/in.h>

#include "hi_unf_avplay.h"
#include "hi_unf_venc.h"
#include "hi_unf_vo.h"
#include "hi_drv_win.h"
#include "hicodec.h"
#include "version.h"

#define TIME_OUT_MS          200
#define AVPLAY_NORMAL_SPEED 1000
#define AVPLAY_PAUSED_SPEED    0

#ifdef __cplusplus
extern "C" {
#endif

#include <dlfcn.h>
HI_S32 HI_MPI_SYNC_Create(HI_UNF_SYNC_ATTR_S *pstSyncAttr, HI_HANDLE *phSync) {
	HI_S32 (*create_sync)(HI_UNF_SYNC_ATTR_S *pstSyncAttr, HI_HANDLE *phSync);

	pstSyncAttr->enSyncRef = HI_UNF_SYNC_REF_NONE;
	pstSyncAttr->stSyncStartRegion.s32VidPlusTime = 0;
	pstSyncAttr->stSyncStartRegion.s32VidNegativeTime = 0;
	pstSyncAttr->stSyncStartRegion.bSmoothPlay = HI_FALSE;
	pstSyncAttr->stSyncNovelRegion.s32VidPlusTime = 0;
	pstSyncAttr->stSyncNovelRegion.s32VidNegativeTime = 0;
	pstSyncAttr->stSyncNovelRegion.bSmoothPlay = HI_FALSE;
	pstSyncAttr->s32VidPtsAdjust = 0;
	pstSyncAttr->s32AudPtsAdjust = 0;
	pstSyncAttr->u32PreSyncTimeoutMs = 1000;
	pstSyncAttr->bQuickOutput = HI_FALSE;

	create_sync = dlsym(RTLD_NEXT, "HI_MPI_SYNC_Create");
	return create_sync(pstSyncAttr, phSync);
}

/**
 * Get Version Info
 *
 *@return: Version Info
 */
const char *vl_get_version() {
	return HICODEC_VERSION;
}

/**
 * Initialize the Encoder
 *
 *@param : codec_id
 *@param :  *codec
 *@param : width
 *@param : height
 *@param : frame_rate
 *@param : bit_rate
 *@param : gop GOP: I-frame maximum interval
 *@return: Return true on success and false on failure.
 */
bool vl_video_encoder_init(SHICODEC *codec, CODEC_ID codec_id, int width, int height, int frame_rate, int bit_rate, int gop) {
	HI_UNF_VENC_CHN_ATTR_S *pstAttr;

	if (!codec)
		return false;

	memset(&codec->hPriv[0], 0, sizeof(codec->hPriv) / sizeof(uint32_t));
	pstAttr = (HI_UNF_VENC_CHN_ATTR_S *)&codec->hPriv[0];

	HI_SYS_Init();
	HI_UNF_VENC_Init();
	HI_UNF_VENC_GetDefaultAttr(pstAttr);

	pstAttr->enVencType= (HI_UNF_VCODEC_TYPE_E)codec_id;
	pstAttr->u32Width  = width / 4 * 4;
	pstAttr->u32Height = height / 4 * 4;
	pstAttr->u32InputFrmRate  = frame_rate;
	pstAttr->u32TargetFrmRate = frame_rate;
	pstAttr->u32Gop = gop;
	pstAttr->bQuickEncode = HI_TRUE;
	pstAttr->bSlcSplitEn  = HI_FALSE;
	if (pstAttr->u32Width > 1280) {
		pstAttr->enCapLevel = HI_UNF_VCODEC_CAP_LEVEL_FULLHD;
		pstAttr->u32StrmBufSize   = 1920 * 1080 * 2;
		pstAttr->u32TargetBitRate = 5 * 1024 * 1024;
	} else if (pstAttr->u32Width > 720) {
		pstAttr->enCapLevel = HI_UNF_VCODEC_CAP_LEVEL_720P;
		pstAttr->u32StrmBufSize   = 1280 * 720 * 2;
		pstAttr->u32TargetBitRate = 3 * 1024 * 1024;
	} else if (pstAttr->u32Width > 352) {
		pstAttr->enCapLevel = HI_UNF_VCODEC_CAP_LEVEL_D1;
		pstAttr->u32StrmBufSize   = 720 * 576 * 2;
		pstAttr->u32TargetBitRate = 3 / 2 * 1024 * 1024;
	} else {
		pstAttr->enCapLevel = HI_UNF_VCODEC_CAP_LEVEL_CIF;
		pstAttr->u32StrmBufSize   = 352 * 288 * 2;
		pstAttr->u32TargetBitRate = 800 * 1024;
	}

	pstAttr->u32TargetBitRate = (0 >= bit_rate) ? pstAttr->u32TargetBitRate : bit_rate;

    if (HI_UNF_VENC_Create(&codec->hPlayer, pstAttr) != HI_SUCCESS) {
		goto ERR0;
	}

	if (HI_UNF_VENC_Start(codec->hPlayer) != HI_SUCCESS) {
		goto ERR1;
	}

	return true;

ERR1:
	HI_UNF_VENC_Destroy(codec->hPlayer);

ERR0:
    return false;
}

HI_MMZ_BUF_S g_stMmzFrm = {0};
HI_S32 transfer_buffer(HI_U8 *in, int length, HI_U32 *pu32PutAddrPhy) {
	HI_S32 s32Ret;
	HI_U32 u32Size=0;
	HI_U32 u32InputAddrPhy = 0;

	s32Ret = HI_MMZ_GetPhyaddr(in, &u32InputAddrPhy, &u32Size);
	if (HI_SUCCESS == s32Ret) {
		*pu32PutAddrPhy = u32InputAddrPhy;
		return HI_SUCCESS;
	}

	if (g_stMmzFrm.bufsize < length) {
		if (0 != g_stMmzFrm.bufsize) {
			s32Ret = HI_MMZ_Free(&g_stMmzFrm);
			if (s32Ret != HI_SUCCESS) {
				return s32Ret;
			}
		}
		snprintf(g_stMmzFrm.bufname, sizeof(g_stMmzFrm.bufname), "hicodec");
		g_stMmzFrm.bufsize = length;
		s32Ret = HI_MMZ_Malloc(&g_stMmzFrm);
		if (s32Ret != HI_SUCCESS) {
			return s32Ret;
		}
	}

	memcpy(g_stMmzFrm.user_viraddr, in, length);
	if (HI_SUCCESS != HI_MMZ_Flush(0)) {
		return HI_FAILURE;
	}
	*pu32PutAddrPhy = g_stMmzFrm.phyaddr;
	return HI_SUCCESS;
}

/**
 * Video Encoding
 *
 *@param : *codec
 *@param : type
 *@param : in
 *@param : in_size
 *@param : out - H.264 need to include (0x00£¬0x00£¬0x00£¬0x01) Start code, internally allocated space (that is, the method allocates space when implemented) and must be in I420 format.
 *@return: Successfully returns the length of the encoded data. Failure returns <= 0
 */
int vl_video_encoder_encode(SHICODEC *codec, FRAME_TYPE type, uint8_t *in, int size, uint8_t **out) {
	HI_S32 i = 0;
	HI_S32 s32Len = 0;
	HI_UNF_VENC_CHN_ATTR_S *pstAttr;
	static HI_UNF_VIDEO_FRAME_INFO_S stFrame = {0};
	static HI_UNF_VENC_STREAM_S stStream = {0};

    if (!codec || codec->hPlayer == HI_INVALID_HANDLE || in == HI_NULL || out == HI_NULL || *out == HI_NULL || size <= 0) {
		goto ERR0;
	}

    if (transfer_buffer((HI_U8*)in, size, &stFrame.stVideoFrameAddr[0].u32YAddr) != HI_SUCCESS) {
		goto ERR0;
	}

	pstAttr = (HI_UNF_VENC_CHN_ATTR_S *)&codec->hPriv[0];
	/* organize cast out frame info */
	stFrame.u32Width  =  pstAttr->u32Width;
	stFrame.u32Height =  pstAttr->u32Height;
	stFrame.stVideoFrameAddr[0].u32CAddr   = stFrame.stVideoFrameAddr[0].u32YAddr + (stFrame.u32Width * stFrame.u32Height);
	stFrame.stVideoFrameAddr[0].u32YStride =  pstAttr->u32Width ;
	stFrame.stVideoFrameAddr[0].u32CStride =  pstAttr->u32Width ;
	stFrame.enVideoFormat = HI_UNF_FORMAT_YUV_SEMIPLANAR_420;
	stFrame.u32AspectWidth  = stFrame.u32Width;
	stFrame.u32AspectHeight = stFrame.u32Width;
	stFrame.stFrameRate.u32fpsDecimal = 0;
	stFrame.stFrameRate.u32fpsInteger = pstAttr->u32TargetFrmRate;
	stFrame.bProgressive = HI_TRUE;
	stFrame.enFieldMode = HI_UNF_VIDEO_FIELD_ALL;
	stFrame.bTopFieldFirst = HI_FALSE;
	stFrame.enFramePackingType = HI_UNF_FRAME_PACKING_TYPE_NONE;
	stFrame.u32Circumrotate   = 0;
	stFrame.bVerticalMirror   = HI_FALSE;
	stFrame.bHorizontalMirror = HI_FALSE;
	stFrame.u32DisplayWidth   = stFrame.u32Width;
	stFrame.u32DisplayHeight  = stFrame.u32Height;
	stFrame.u32DisplayCenterX = stFrame.u32Width / 2;
	stFrame.u32DisplayCenterY = stFrame.u32Height / 2;
	stFrame.u32ErrorLevel = 0;
	stFrame.u32FrameIndex++;

	if (HI_SYS_GetTimeStampMs(&stFrame.u32Pts) != HI_SUCCESS)
		stFrame.u32Pts = HI_INVALID_PTS;
	stFrame.u32SrcPts = stFrame.u32Pts;

	if (stStream.pu8Addr != HI_NULL) {
		HI_UNF_VENC_ReleaseStream(codec->hPlayer, &stStream);
		stStream.pu8Addr = HI_NULL;
	}

	if (type == FRAME_TYPE_I) {
		if (HI_UNF_VENC_RequestIFrame(codec->hPlayer) != HI_SUCCESS) {
			goto ERR0;
		}
	}

	i=0;
	while (HI_UNF_VENC_QueueFrame(codec->hPlayer, &stFrame) != HI_SUCCESS) {
		usleep(1000);
		if (TIME_OUT_MS <= i++) {
			goto ERR0;
		}
	}

	i=0;
	s32Len = 0;
	while (1) {
		if (HI_UNF_VENC_AcquireStream(codec->hPlayer, &stStream, 0) != HI_SUCCESS) {
			usleep(1000);
			if (TIME_OUT_MS <= i++) {
				s32Len = -1;
				goto EXIT0;
			}
		} else {
			if (stStream.bFrameEnd == HI_TRUE && s32Len == 0) {
				s32Len = stStream.u32SlcLen;
				memcpy(*out, stStream.pu8Addr, stStream.u32SlcLen);
				// * out = (char *)stStream.pu8Addr;
				break;
			}

			/*if (0x80000 < (s32Len + stStream.u32SlcLen)) {
				s32Len = -1;
				goto EXIT0;
			}*/

			memcpy((*out + s32Len), stStream.pu8Addr, stStream.u32SlcLen);
			s32Len += stStream.u32SlcLen;

			HI_UNF_VENC_ReleaseStream(codec->hPlayer, &stStream);
			stStream.pu8Addr = HI_NULL;
			if(HI_TRUE == stStream.bFrameEnd) {
			// * out = (char *)u8StreamBuffer;
				break;
			}
		}
	}

EXIT0:
	HI_UNF_VENC_DequeueFrame(codec->hPlayer, &stFrame);
	return s32Len;

ERR0:
	return HI_FAILURE;
}

/**
 * Destroy the Encoder
 *
 *@param : *codec
 */
void vl_video_encoder_destroy(SHICODEC *codec) {
	if (!codec || codec->hPlayer == HI_INVALID_HANDLE)
		return;

	HI_UNF_VENC_Stop(codec->hPlayer);
	HI_UNF_VENC_Destroy(codec->hPlayer);
	HI_UNF_VENC_DeInit();

	if (g_stMmzFrm.bufsize != 0) {
		HI_MMZ_Free(&g_stMmzFrm);
		memset(&g_stMmzFrm, 0, sizeof(HI_MMZ_BUF_S));
	}

	codec->hPlayer = HI_INVALID_HANDLE;
}

/**
 * Internal Decoder Video Event
 */
static uint32_t event_window = HI_INVALID_HANDLE;
static HICODEC_FRAME_EVENT event_callback = NULL;
int vl_video_decoder_video_event(HI_HANDLE handle, HI_UNF_AVPLAY_EVENT_E enEvent, HI_VOID *para) {
	if (event_callback) {
		HI_U8 *buffer;
		HI_U8 *pBufVirAddr;
		HI_UNF_VIDEO_FRAME_INFO_S *vFrame = (HI_UNF_VIDEO_FRAME_INFO_S *)para;
		HI_S32 u32Size = vFrame->u32Height * vFrame->u32Width * 3 / 2;

		if (!(buffer = (HI_U8 *)malloc(u32Size))) {
			HI_UNF_VO_ReleaseFrame(event_window, vFrame);
			return -1;
		} else if (!(pBufVirAddr = HI_MMZ_Map(vFrame->stVideoFrameAddr[0].u32YAddr, HI_FALSE))) {
			HI_UNF_VO_ReleaseFrame(event_window, vFrame);
			free(buffer);
			return -1;
		}

		memcpy(buffer, pBufVirAddr, u32Size);

		event_callback(vFrame->u32Width, vFrame->u32Height, buffer, u32Size, vFrame->u32Pts);

		HI_MMZ_Unmap(vFrame->stVideoFrameAddr[0].u32YAddr);
		HI_UNF_VO_ReleaseFrame(event_window, vFrame);
		free(buffer);
	}

	return 0;
}

/**
 * Initialize the Decoder
 *
 *@param : codec_id
 *@param : *codec
 *@return: Return true on success and false on failure.
 */
bool vl_video_decoder_init(CODEC_ID codec_id, SHICODEC *codec) {
	HI_UNF_AVPLAY_ATTR_S stAvAttr;
	HI_UNF_AVPLAY_OPEN_OPT_S stAvOpt;
	HI_UNF_VCODEC_ATTR_S stAvVdecAttr;
	HI_UNF_WINDOW_ATTR_S stVirWinAttr;

	if (!codec)
		return false;

	HI_SYS_Init();

	if (HI_UNF_VO_Init(HI_UNF_VO_DEV_MODE_NORMAL) != HI_SUCCESS)
		return false;
	else if (HI_UNF_AVPLAY_Init() != HI_SUCCESS) {
		HI_UNF_VO_DeInit();
		return false;
	}

	if (HI_UNF_AVPLAY_GetDefaultConfig(&stAvAttr, HI_UNF_AVPLAY_STREAM_TYPE_ES) != HI_SUCCESS)
		goto ERR0;
	stAvAttr.stStreamAttr.u32VidBufSize = 16 * 1024 * 1024; // Allocate 16MB

	if (HI_UNF_AVPLAY_Create(&stAvAttr, &codec->hPlayer) != HI_SUCCESS)
		goto ERR0;

	stAvOpt.enDecType  = HI_UNF_VCODEC_DEC_TYPE_NORMAL;
	stAvOpt.enCapLevel = HI_UNF_VCODEC_CAP_LEVEL_4096x2160; // HI_UNF_VCODEC_CAP_LEVEL_FULLHD
	stAvOpt.enProtocolLevel = HI_UNF_VCODEC_PRTCL_LEVEL_H264;

	if (HI_UNF_AVPLAY_ChnOpen(codec->hPlayer, HI_UNF_AVPLAY_MEDIA_CHAN_VID, &stAvOpt) != HI_SUCCESS)
		goto ERR1;

	if (HI_UNF_AVPLAY_GetAttr(codec->hPlayer, HI_UNF_AVPLAY_ATTR_ID_VDEC, &stAvVdecAttr) != HI_SUCCESS)
		goto ERR2;

	stAvVdecAttr.enType = (HI_UNF_VCODEC_TYPE_E)codec_id;
	stAvVdecAttr.enMode = HI_UNF_VCODEC_MODE_NORMAL;
	stAvVdecAttr.u32ErrCover  = 100;
	stAvVdecAttr.u32Priority  = 3;

	if (HI_UNF_AVPLAY_SetAttr(codec->hPlayer, HI_UNF_AVPLAY_ATTR_ID_VDEC, &stAvVdecAttr) != HI_SUCCESS)
		goto ERR2;

	memset(&stVirWinAttr, 0, sizeof(HI_UNF_WINDOW_ATTR_S));

	stVirWinAttr.enDisp   = HI_UNF_DISPLAY1;
	stVirWinAttr.bVirtual = HI_TRUE;
	stVirWinAttr.enVideoFormat = HI_UNF_FORMAT_YUV_SEMIPLANAR_420;
	stVirWinAttr.stWinAspectAttr.enAspectCvrs = HI_UNF_VO_ASPECT_CVRS_IGNORE;
	stVirWinAttr.stWinAspectAttr.bUserDefAspectRatio = HI_FALSE;
	stVirWinAttr.stWinAspectAttr.u32UserAspectWidth  = 0;
	stVirWinAttr.stWinAspectAttr.u32UserAspectHeight = 0;
	stVirWinAttr.bUseCropRect = HI_FALSE;
	memset(&stVirWinAttr.stInputRect, 0x0, sizeof(HI_RECT_S));
	memset(&stVirWinAttr.stOutputRect, 0x0, sizeof(HI_RECT_S));

	if (HI_UNF_VO_CreateWindow(&stVirWinAttr, &codec->hWindow) != HI_SUCCESS)
		goto ERR3;

	if (HI_UNF_VO_AttachWindow(codec->hWindow, codec->hPlayer) != HI_SUCCESS)
		goto ERR4;

	if (HI_UNF_VO_SetWindowEnable(codec->hWindow, HI_TRUE) != HI_SUCCESS)
		goto ERR5;

	if (HI_UNF_AVPLAY_RegisterEvent(codec->hPlayer, HI_UNF_AVPLAY_EVENT_NEW_VID_FRAME, (HI_UNF_AVPLAY_EVENT_CB_FN)vl_video_decoder_video_event) != HI_SUCCESS)
		goto ERR5;

	if (HI_UNF_AVPLAY_Start(codec->hPlayer, HI_UNF_AVPLAY_MEDIA_CHAN_VID, NULL) != HI_SUCCESS)
		goto ERR6;

	vl_video_decoder_set_fps(codec, 0);
	codec->hDisplay = HI_INVALID_HANDLE;
	codec->hCodec = codec_id;
	codec->LastSpeed = AVPLAY_NORMAL_SPEED;
	codec->TotalInputs = 0;
	codec->TotalOutputs= 0;
	return true;

ERR6:
	HI_UNF_AVPLAY_UnRegisterEvent(codec->hPlayer, HI_UNF_AVPLAY_EVENT_NEW_VID_FRAME);

ERR5:
	HI_UNF_VO_SetWindowEnable(codec->hWindow, HI_FALSE);

ERR4:
	HI_UNF_VO_DetachWindow(codec->hWindow, codec->hPlayer);

ERR3:
	HI_UNF_VO_DestroyWindow(codec->hWindow);

ERR2:
	HI_UNF_AVPLAY_ChnClose(codec->hPlayer, HI_UNF_AVPLAY_MEDIA_CHAN_VID);

ERR1:
	HI_UNF_AVPLAY_Destroy(codec->hPlayer);

ERR0:
	codec->hDisplay = HI_INVALID_HANDLE;
	codec->hPlayer = HI_INVALID_HANDLE;
	codec->hWindow = HI_INVALID_HANDLE;
	HI_UNF_AVPLAY_DeInit();
	HI_UNF_VO_DeInit();

    return false;
}

/**
 * Set Decoder FPS
 *
 *@param : *codec
 *@param : fps
 *@return: Return true on success and false on failure.
 */
bool vl_video_decoder_set_fps(SHICODEC *codec, int fps) {
	HI_UNF_AVPLAY_FRMRATE_PARAM_S stFrmRateAttr;

	if (!codec || codec->hPlayer == HI_INVALID_HANDLE)
		return false;

	stFrmRateAttr.enFrmRateType = (fps > 0) ? HI_UNF_AVPLAY_FRMRATE_TYPE_USER : HI_UNF_AVPLAY_FRMRATE_TYPE_PTS;
	stFrmRateAttr.stSetFrmRate.u32fpsInteger = (fps > 0) ? fps / 1000 : 0;
	stFrmRateAttr.stSetFrmRate.u32fpsDecimal = (fps > 0) ? fps % 1000 : 0;
	if (HI_UNF_AVPLAY_SetAttr(codec->hPlayer, HI_UNF_AVPLAY_ATTR_ID_FRMRATE_PARAM, &stFrmRateAttr) != HI_SUCCESS)
		return false;

	return true;
}

/**
 * Enable Video Decoder Output Window (Only works with: vl_video_decoder_output)
 *
 *@param : *codec
 *@return: Return true on success and false on failure.
 */
bool vl_video_decoder_display_init(SHICODEC *codec) {
	unsigned int h, w;
	HI_UNF_WINDOW_ATTR_S WinAttr;

	if (!codec || codec->hPlayer == HI_INVALID_HANDLE || codec->hDisplay != HI_INVALID_HANDLE)
		return false;

	if (HI_UNF_VO_Init(HI_UNF_VO_DEV_MODE_NORMAL) != HI_SUCCESS)
		return false;

	if (HI_UNF_DISP_Init() != HI_SUCCESS)
		return false;

	if (HI_UNF_DISP_GetVirtualScreen(HI_UNF_DISPLAY1, &w, &h) != HI_SUCCESS)
		return false;
	else
	{
		char modes[24];
		HI_UNF_DISP_HDR_TYPE_E mode = HI_UNF_DISP_HDR_TYPE_NONE;
		FILE *file = fopen("/proc/stb/video/hdmi_hdrtype", "r");

		if (file) {
			fgets(modes, sizeof(modes), file);
			fclose(file);

			if (strstr(modes, "dolby"))
				mode = HI_UNF_DISP_HDR_TYPE_DOLBY;
			else if (strstr(modes, "hdr10"))
				mode = HI_UNF_DISP_HDR_TYPE_HDR10;
		}

		if (mode != HI_UNF_DISP_HDR_TYPE_NONE) {
			HI_UNF_DISP_HDR_TYPE_E mCur;
			if (HI_UNF_DISP_GetHDRType(HI_UNF_DISPLAY1, &mCur) == HI_SUCCESS && mCur != mode)
				HI_UNF_DISP_SetHDRType(HI_UNF_DISPLAY1, mode);
		}
	}

	memset(&WinAttr, 0, sizeof(HI_UNF_WINDOW_ATTR_S));
	WinAttr.enDisp = HI_UNF_DISPLAY1;
	WinAttr.bVirtual = HI_FALSE;
	WinAttr.stWinAspectAttr.enAspectCvrs = HI_UNF_VO_ASPECT_CVRS_IGNORE;
	WinAttr.stWinAspectAttr.bUserDefAspectRatio = HI_FALSE;
	WinAttr.stWinAspectAttr.u32UserAspectWidth  = 0;
	WinAttr.stWinAspectAttr.u32UserAspectHeight = 0;
	WinAttr.bUseCropRect = HI_FALSE;
	WinAttr.stInputRect.s32X = 0;
	WinAttr.stInputRect.s32Y = 0;
	WinAttr.stInputRect.s32Width = 0;
	WinAttr.stInputRect.s32Height = 0;
	WinAttr.stOutputRect.s32X = 0;
	WinAttr.stOutputRect.s32Y = 0;
	WinAttr.stOutputRect.s32Width = w - 2;
	WinAttr.stOutputRect.s32Height= h - 4;

	if (HI_UNF_VO_CreateWindow(&WinAttr, &codec->hDisplay) != HI_SUCCESS)
		goto vo_deinit;

	HI_UNF_VO_AttachWindow(codec->hDisplay, codec->hPlayer);
	if (HI_UNF_VO_SetWindowEnable(codec->hDisplay, HI_TRUE) != HI_SUCCESS)
		goto win_destroy;

	return true;

win_destroy:
	HI_UNF_VO_DestroyWindow(codec->hDisplay);

vo_deinit:
	HI_UNF_VO_DeInit();

	codec->hDisplay = HI_INVALID_HANDLE;
	return false;
}

/**
 * Mode of Video Decoder Output Window (Only works after: vl_video_decoder_display_init)
 *
 *@param : *codec
 *@return: Return true on success and false on failure.
 */
bool vl_video_decoder_trickplay(SHICODEC *codec, int32_t speed) {
	HI_UNF_AVPLAY_TPLAY_OPT_S pSpeed;
	int32_t LastSpeed = codec->LastSpeed;

	if (!codec || codec->hPlayer == HI_INVALID_HANDLE || codec->hDisplay == HI_INVALID_HANDLE)
		return false;

	codec->LastSpeed = speed;
	if (speed == AVPLAY_PAUSED_SPEED) {
		HI_UNF_AVPLAY_Pause(codec->hPlayer, HI_NULL);

		if (LastSpeed == AVPLAY_NORMAL_SPEED && codec->TotalOutputs < 2) {        // This will can run only when video start or resumed from point.
			HI_UNF_AVPLAY_Reset(codec->hPlayer, HI_NULL);
		} else if (LastSpeed == AVPLAY_PAUSED_SPEED && codec->TotalOutputs > 2) { // This will can run only when direct seek by pointer
			HI_UNF_AVPLAY_Reset(codec->hPlayer, HI_NULL);
		}

		return true;
	}

	pSpeed.u32SpeedInteger = abs(speed) / AVPLAY_NORMAL_SPEED;
	pSpeed.u32SpeedDecimal = abs(speed) % AVPLAY_NORMAL_SPEED;
	pSpeed.enTplayDirect   = (speed > 0) ? HI_UNF_AVPLAY_TPLAY_DIRECT_FORWARD : HI_UNF_AVPLAY_TPLAY_DIRECT_BACKWARD;

	if (speed == AVPLAY_NORMAL_SPEED) {
		if (LastSpeed != AVPLAY_NORMAL_SPEED && LastSpeed != AVPLAY_PAUSED_SPEED) {
			HI_UNF_AVPLAY_Tplay(codec->hPlayer, &pSpeed);
			HI_UNF_AVPLAY_SetDecodeMode(codec->hPlayer, HI_UNF_VCODEC_MODE_NORMAL);
		} else  {
			HI_UNF_AVPLAY_Resume(codec->hPlayer, HI_NULL);
		}

		return true;
	}

	HI_UNF_AVPLAY_SetDecodeMode(codec->hPlayer, HI_UNF_VCODEC_MODE_IP);
	return (HI_UNF_AVPLAY_Tplay(codec->hPlayer, &pSpeed) == HI_SUCCESS);
}

/**
 * Video Decoding
 *
 *@param : *codec
 *@param : in
 *@param : in_size
 *@param : out
 *@return: Successfully returns the length of the decoded data. Failure returns <= 0
 */
int vl_video_decoder_decode(SHICODEC *codec, uint8_t *in, int size, uint8_t **out) {
	if (!codec || out == HI_NULL || *out == HI_NULL)
		return -1;

	if (vl_video_decoder_input(codec, in, size, 0)) {
		int i = 0;
		int oSize = 0;
		int64_t pts = 0;

		while ((oSize = vl_video_decoder_output(codec, out, &pts)) < 0) {
			usleep(1000);
			if (TIME_OUT_MS <= i++) {
				return -1;
			}
		}

		return oSize;
	}

	return -1;
}

/**
 * Input Video Buffer for Decoding
 *
 *@param : *codec
 *@param : in
 *@param : size
 *@param : pts
 *@return: Successfully returns true and false on failure
 */
bool vl_video_decoder_input(SHICODEC *codec, uint8_t *in, int size, int64_t pts) {
	HI_UNF_STREAM_BUF_S stAvplayBuf;

	if (!codec || codec->hPlayer == HI_INVALID_HANDLE || !in)
		return false;

	if (HI_UNF_AVPLAY_GetBuf(codec->hPlayer, HI_UNF_AVPLAY_BUF_ID_ES_VID, size, &stAvplayBuf, 0) == HI_SUCCESS) {
		memcpy((HI_VOID*)stAvplayBuf.pu8Data, (HI_VOID*)in, size);

		if (HI_UNF_AVPLAY_PutBuf(codec->hPlayer, HI_UNF_AVPLAY_BUF_ID_ES_VID, size, (pts <= 0) ? 0 : pts) != HI_SUCCESS)
			return false;

		codec->TotalInputs++;
		return true;
	}

	return false;
}

/**
 * Output Video Buffer Decoded
 *
 *@param : *codec
 *@param : out
 *@param : pts
 *@return: Successfully returns the length of the decoded data. Failure returns < 0
 */
int vl_video_decoder_output(SHICODEC *codec, uint8_t **out, int64_t *pts) {
	HI_S32 u32Size;
	HI_UNF_VIDEO_FRAME_INFO_S stFrameinfo;

	if (!codec || codec->hPlayer == HI_INVALID_HANDLE || codec->hWindow == HI_INVALID_HANDLE || ((out == HI_NULL || *out == HI_NULL) && codec->hDisplay == HI_INVALID_HANDLE))
		return -1;

	if (HI_UNF_VO_AcquireFrame(codec->hWindow, &stFrameinfo, 0) != HI_SUCCESS)
		return -1;

	*pts = stFrameinfo.u32Pts;
	u32Size = stFrameinfo.u32Height * stFrameinfo.u32Width * 3 / 2;

	if (out != HI_NULL && *out != HI_NULL) {
		HI_U8 *pBufVirAddr;

		if (!(pBufVirAddr = HI_MMZ_Map(stFrameinfo.stVideoFrameAddr[0].u32YAddr, HI_FALSE))) {
			HI_UNF_VO_ReleaseFrame(codec->hWindow, &stFrameinfo);
			return -1;
		}

		memcpy(*out, pBufVirAddr, u32Size);
		HI_MMZ_Unmap(stFrameinfo.stVideoFrameAddr[0].u32YAddr);
	} else if (codec->hDisplay == HI_INVALID_HANDLE) {
		return -1;
	}

	HI_UNF_VO_ReleaseFrame(codec->hWindow, &stFrameinfo);
	codec->TotalOutputs++;
	return u32Size;
}

/**
 * Callback for Output Video Buffer Decoded
 *
 *@param : *codec
 *@param : callback
 *@return: Successfully returns true and false on failure
 */
bool vl_video_decoder_register_event(SHICODEC *codec, HICODEC_FRAME_EVENT callback) {
	if (!codec || codec->hPlayer == HI_INVALID_HANDLE || codec->hWindow == HI_INVALID_HANDLE)
		return false;

	event_window = codec->hWindow;
	event_callback = callback;
	return true;
}

/**
 * Destroy the Decoder
 *@param : *codec
 */
void vl_video_decoder_destroy(SHICODEC *codec) {
	HI_UNF_AVPLAY_STOP_OPT_S Stop;

	if (!codec || codec->hPlayer == HI_INVALID_HANDLE || codec->hWindow == HI_INVALID_HANDLE)
		return;

	Stop.enMode = HI_UNF_AVPLAY_STOP_MODE_BLACK;
	Stop.u32TimeoutMs = 0;
	HI_UNF_AVPLAY_Stop(codec->hPlayer, HI_UNF_AVPLAY_MEDIA_CHAN_VID, &Stop);
	HI_UNF_AVPLAY_UnRegisterEvent(codec->hPlayer, HI_UNF_AVPLAY_EVENT_NEW_VID_FRAME);

	HI_UNF_VO_SetWindowEnable(codec->hWindow, HI_FALSE);
	HI_UNF_VO_DetachWindow(codec->hWindow, codec->hPlayer);
	HI_UNF_VO_DestroyWindow(codec->hWindow);

	if (codec->hDisplay != HI_INVALID_HANDLE) {
		HI_UNF_VO_SetWindowEnable(codec->hDisplay, HI_FALSE);
		HI_UNF_VO_DetachWindow(codec->hDisplay, codec->hPlayer);
		HI_UNF_VO_DestroyWindow(codec->hDisplay);
		HI_UNF_DISP_DeInit();
	}

	HI_UNF_AVPLAY_ChnClose(codec->hPlayer, HI_UNF_AVPLAY_MEDIA_CHAN_VID);
	HI_UNF_AVPLAY_Destroy(codec->hPlayer);

	codec->hDisplay = HI_INVALID_HANDLE;
	codec->hPlayer = HI_INVALID_HANDLE;
	codec->hWindow = HI_INVALID_HANDLE;
	codec->TotalInputs = 0;
	codec->TotalOutputs= 0;
	event_callback = NULL;
	event_window = HI_INVALID_HANDLE;

	HI_UNF_AVPLAY_DeInit();
	HI_UNF_VO_DeInit();
}

#ifdef __cplusplus
}
#endif
